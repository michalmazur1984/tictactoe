# TicTacToe



## To Do
- Zbuduj klasę dziedziczącą po JFrame
- Zbuduj klasę dziedzicząco po JPanel
- Podepnij panel pod frame
- Zajmplementuj metodę paintComponent
- Zdefiniuj rozmary okna
- Narysuj podziałkę planszy 3x3 za pomocą obiektu Graphics2D w metodzie paintComponent
- Narysuj O lub X o dużych rozmiarach w miejscu kliknięcia myszą
    Do tego będziesz potrzebował:
  - Stworzyć klase implementującą Action Listener
  - Przesłonić metodę actionPerformed
  - Stworzyć obiekt powyższej klasy i dodać go jako actionListener do panelu
  - Użyć metod z klasy Graphics2D do narysowania kształtów albo tekstu
  - Ustawić odpowiednią wielkość, proporcjonalną do planszy
- Dopasuj kliknięcie "kwadratu" w którym się znajduje
- Zrób tablicę dwuwymiarową 3x3 przechowującą informacje o symbolach dla danego pola (Mogą być Enumy)
- Po kliknięciu zrób weryfikację czy stworzyła się linia z trzech O albo X.
- DOdaj weryfikację na temat liczby uzupełnionych pól tak aby w przypadku remisu zakończyc gre i poinformować o tym uczestników
- 

