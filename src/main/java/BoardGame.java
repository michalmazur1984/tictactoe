import javax.swing.*;
import java.awt.*;

public class BoardGame extends JPanel {

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.drawLine(0,0, BoardFrame.SCREEN_SIZE.width, BoardFrame.SCREEN_SIZE.height);
    }
}
